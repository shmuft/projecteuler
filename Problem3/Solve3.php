<?php
/**
 * Created by PhpStorm.
 * User: Иванус
 * Date: 01.04.2016
 * Time: 20:12
 */

namespace Euler;


class Solve3
{
    private static function getFactorsNumber($number){
        $primeFactors = [];

        while (self::isMultiple($number,2)){
            $primeFactors[] = 2;
            $number = $number / 2;
        }

        for ($i=3;$number/2>$i;$i+=2){
            while (self::isMultiple($number,$i)){
                $primeFactors[] = $i;
                $number = $number / $i;
            }
        }
        $primeFactors[] = intval($number);
        return $primeFactors;
    }

    public static function solve($number)
    {
        return max(self::getFactorsNumber($number));
    }

    private static function isMultiple($number, $basis)
    {
        if (gmp_mod($number, $basis) === 0) {
            return true;
        }
        return false;
    }
}

echo PHP_INT_MAX;

echo Solve3::solve(600851475143);