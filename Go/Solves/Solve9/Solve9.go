package main

import "fmt"

func piphagor (s int)(int, int, int){
	for a:=1; a < s/2; a++ {
		for b:= a+1; b<s/2; b++ {
			c := s-a-b;
			if a*a + b*b == c*c {
				return a, b, c
			}
		}
	}
	return 0,0,0
}

func main() {
	a, b, c := piphagor(1000)
	fmt.Printf("a = %d, b = %d, c = %d",a ,b, c)
}
