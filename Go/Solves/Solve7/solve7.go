package main

import (
	"fmt"
	"time"
	"math"
)

func isPrimeNumber(num int64, basis int64) bool {
	if math.Mod(float64(num), float64(basis))==0 {
		return true
	}
	return false
}

func NPrimeNumber(n int64) int64 {
	var PrimeNumbers []int64
	var i, j int64
	var hopHayLalalay bool
	i = 1
	PrimeNumbers = append(PrimeNumbers, 2)
	for j=3; ;j+=2{
		if (i == n) {
			break
		}
		hopHayLalalay = false
		for _, k := range PrimeNumbers {
			if (isPrimeNumber(j,k)) {
				hopHayLalalay = true
				break
			}
		}
		if hopHayLalalay {
			continue
		}
		PrimeNumbers = append(PrimeNumbers, j)
		i++
	}
	return PrimeNumbers[n-1]
}

func main() {
	var n int64
	n = 10001
	t0 := time.Now()
	NPrime := NPrimeNumber(n)
	t1 := time.Now()

	fmt.Printf("%vst prime number is %v.\n",n, NPrime)
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))
}
