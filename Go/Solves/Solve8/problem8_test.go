package main

import "testing"

func TestMaxProduct(t *testing.T){
	var tests = []struct {
		input int
		want int
	}{
		{4, 5832},
		//{8, 5831},
	}
	for _, test := range tests {
		if got := MaxProduct(test.input); got != test.want {
			t.Errorf("MaxProduct(%v) = %v", test.input, got)
		}
	}

}

