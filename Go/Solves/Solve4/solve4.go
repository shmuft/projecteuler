package main

import(
	"fmt"
	"math"
	"strconv"
	"time"
)

func buildPalindromicNumber(number int) int32{
	s := strconv.Itoa(number)
	var s1 string
	s1 = s
	for i:=len(s);i>0;i-- {
		s1 = s1 + s[i-1:i]
	}
	ii, err := strconv.Atoi(s1)
	if err != nil {

	}
	return int32(ii)
}
// We need find x from formula x=z/y.
func isRightNumber(number int32) bool{
	for y:=999; y>0; y=y-2 {
		if math.Mod(float64(number), float64(y)) == 0{
			if y>99 && (number/int32(y)>99) && (number/int32(y)<1000) {
				//fmt.Printf("Number = %v, x=%v, y=%v.\n", number, number / int32(y), y)
				return true
			}
		}
	}
	return false
}

func findPalindromicNumber(start int) int32 {
	var maxNumber int32

	maxNumber = 0
	for leftStartNumber := start;leftStartNumber > 0;leftStartNumber = leftStartNumber - 1{
		palindromicNumber := buildPalindromicNumber(leftStartNumber)
		if isRightNumber(palindromicNumber){
			if maxNumber < palindromicNumber {
				maxNumber = palindromicNumber
			}
		}
	}

	return maxNumber
}

func main() {
	t0 := time.Now()
	fmt.Printf("Palindromic number is %v\n", findPalindromicNumber(999))
	t1 := time.Now()
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))

}
