package main

import (
	"fmt"

	"math"
	"time"
)

func findSmallestNumber(number int64) int64 {
	var i, multip int64
	multip = 1;
	for i=1; i<=number; i++{
		if (math.Mod(float64(multip),float64(i)) != 0){
			for j:=int64(1); j <= i; j++{
				if (math.Mod(float64(multip * j),float64(i))==0){
					multip = multip * j
					break
				}
			}
		}
	}
	return multip
}
func main() {
	t0 := time.Now()
	smallest := findSmallestNumber(20)

	fmt.Printf("Smallest multiply is %v.", smallest)
	t1 := time.Now()
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))

}
