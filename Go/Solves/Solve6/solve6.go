package main

import (
	"fmt"
	"time"
)

func findDifference(number int64) int64 {
	var sum, diff int64
	sum = number
	for i:=int64(number); i>1; i-- {
		diff += sum * (i-1)
		sum += i-1
	}
	diff = diff * 2
	return diff
}

func main() {
	t0 := time.Now()
	diff := findDifference(100)
	t1 := time.Now()

	fmt.Printf("Difference is %v.\n", diff)
	fmt.Printf("The call took %v to run.\n", t1.Sub(t0))

}
