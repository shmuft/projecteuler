package main

import (
	"math"
	"fmt"
	"time"
	"flag"
)

func isPrimeNumber(num, basis int) bool {
	if math.Mod(float64(num), float64(basis))==0 {
		return false
	}
	return true
}

func spinner(delay time.Duration){
	for {
		for _, r := range `-\|/` {
			fmt.Printf("\r%c", r)
			time.Sleep(delay)
		}
	}
}

var maxNumber = flag.Int("number", 10, "maximum number")

func main() {
	flag.Parse()
	var number int
	number = *maxNumber

	primeNumbers := make(chan int, 10)
	notPrimeNumbers := make(map[int]bool, 0)
	var i int
	go spinner(100 * time.Microsecond)
	go func(){
		defer close(primeNumbers)
		primeNumbers <- 2
		for i = 3; i <= number; i += 2 {
			if _, ok := notPrimeNumbers[i]; !ok {
				primeNumbers <- i
				for j := 3; i * j < number; j += 2 {
					notPrimeNumbers[i * j] = true
				}
			}
		}
	}()
	var sum int = 0
	for prime := range primeNumbers {
		sum += prime
	}
	fmt.Printf("Sum primes is %d. Number is %d.", sum, number)
}
