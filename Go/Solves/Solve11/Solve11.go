package main

import (
	"bufio"
	"os"
	"fmt"
	"io"
)

const sizeN = 20
var grid [sizeN][sizeN]int
var ch = make(chan int, 4)

func upDown(){
	var maxProd = 0
	for i := 0; i<sizeN; i++{
		for j:=0; j<sizeN-4; j++{
			prod := 1
			for k:=0; k<4; k++{
				prod *= grid[j+k][i]
			}
			if prod>maxProd {
				maxProd = prod
			}
		}
	}
	ch <- maxProd
}

func leftRight(){
	var maxProd = 0
	for i := 0; i<sizeN; i++{
		for j:=0; j<sizeN-4; j++{
			prod := 1
			for k:=0; k<4; k++{
				prod *= grid[i][j+k]
			}
			if prod>maxProd {
				maxProd = prod
			}
		}
	}
	ch <- maxProd
}

func mainDiag(){
	var maxProd = 0
	for i := 0; i<=sizeN-4; i++{
		for j:=0; j<=sizeN-4; j++{
			prod := 1
			for k:=0; k<4; k++{
				prod *= grid[i+k][j+k]
			}
			if prod>maxProd {
				maxProd = prod
			}
		}
	}
	ch <- maxProd
}

func notMainDiag(){
	var maxProd = 0
	for i := 0; i<=sizeN-4; i++{
		for j:=3; j<sizeN; j++{
			prod := 1
			for k:=0; k<4; k++{
				prod *= grid[i+k][j-k]
			}
			if prod>maxProd {
				maxProd = prod
			}
		}
	}
	ch <- maxProd
}

func main() {
	f, err := os.Open("Go/Solves/Solve11/grid.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	reader := bufio.NewReader(f)
	i := 0
	for {
		j := 0
		line, _, err := reader.ReadLine();

		if err == io.EOF {
			fmt.Println("File loaded")
			break
		}
		dec := 10
		for _, item := range line {

			if item != 32 {
				grid[i][j] += (int(item) - 48) * dec
				if dec == 10 {
					dec = 1

				}else{
					j++
					dec = 10
				}
			}
		}
		i++
	}
	go upDown()
	go leftRight()
	go mainDiag()
	go notMainDiag()
	i = 0
	var maxProd int = 0
	for prod := range ch{
		if prod > maxProd {
			maxProd = prod
		}
		i++
		if i == 4 {
			close(ch)
		}
	}
	fmt.Println(maxProd)
}

