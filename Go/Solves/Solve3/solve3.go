package main

import(
	"fmt"
	"math"
)

func isPrimeNumber(num, basis int64) bool {
	if math.Mod(float64(num), float64(basis))==0 {
		return true	
	}
	return false
}

func MaxPrimeNumber(num int64) int64 {

	var maxPrimeNumber int64
	maxPrimeNumber = 0
	
	if isPrimeNumber(num, 2) {
		num = num / 2
		maxPrimeNumber = 2
	}
	var i int64

	for i = 3; int64(i)<=num; i = i + 2 {
		if isPrimeNumber(num, i) {
			fmt.Printf("num is %v. I is %v. \n", num, i)
			num = num / i
			maxPrimeNumber = i
		}
	}


	return maxPrimeNumber
	
}

func main(){
	fmt.Printf("Max Prime number is %v.",MaxPrimeNumber(600851475143))
}